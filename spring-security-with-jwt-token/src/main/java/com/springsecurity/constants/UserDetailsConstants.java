package com.springsecurity.constants;

import lombok.Data;

@Data
public class UserDetailsConstants {

	private UserDetailsConstants() {

	}

	public static final String USER_REGISTRATION_IS_DONE_SUCCESSFULLY = "User Registration is Done Successfully";

	public static final String ROLE = "ROLE_";

	public static final String USER_NOT_FOUND_EXCEPTION = "User Not Found Exception";

}
