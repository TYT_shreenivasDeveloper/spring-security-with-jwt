package com.springsecurity.constants;

import lombok.Data;

@Data
public class ProductContants {

	private ProductContants() {

	}

	public static final String PRODUCT_DETAILS_DELETED_S_UCCESSFULLY = "Product Details Deleted SUccessfully";

	public static final String PRODUCT_DETAILS_NOT_FOUND = "Product Details Not Found";

	public static final String PRODUCT_ADDED_SUCCESSFULLY = "Product Added Successfully";

	public static final String SOMETHING_WENT_WRONG = "Something Went Wrong";
	
	public static final String GETTING_THE_DETAILS_BASED_ON_PRODUCT_ID = "Getting the details based on productId";
	
	public static final String PRODUCT_DETAILS_DELETE_SUCCESSFULLY = "Product Details Delete Successfully";
	
	public static final String PRODUCT_DETAILS_UPDATED_SUCCESSFULLY = "Product Details Updated Successfully";
	
	public static final String GETTING_ALL_THE_DETAILS = "Getting all the details";

}
