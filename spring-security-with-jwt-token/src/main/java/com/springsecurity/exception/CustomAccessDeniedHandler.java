package com.springsecurity.exception;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springsecurity.response.Response;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			org.springframework.security.access.AccessDeniedException accessDeniedException)
			throws IOException, ServletException {
		try {
			response.setHeader("Error", accessDeniedException.getMessage());
			response.setStatus(HttpStatus.FORBIDDEN.value());
			log.error(accessDeniedException.getMessage());
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			new ObjectMapper().writeValue(response.getOutputStream(),
					Response.builder().error(Boolean.TRUE).message(accessDeniedException.getMessage()).build());
		} catch (Exception exception2) {
			throw new ProductNotFoundException("Something Went Wrong");
		}
	}
}
