package com.springsecurity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springsecurity.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

	public Product findByProductIdAndIsDeleted(String productId, Boolean isDeleted);

	public List<Product> findAllByIsDeleted(Boolean isDeleted);

}
