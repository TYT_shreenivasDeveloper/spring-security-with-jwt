package com.springsecurity.handler;

import java.io.IOException;
import java.util.HashMap;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springsecurity.exception.ProductNotFoundException;
import com.springsecurity.response.Response;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class Exceptionhandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Response> constraintViolationException(ConstraintViolationException exception,
			WebRequest request) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(Response.builder().error(true).message(exception.getMessage()).build());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> internalServerError(RuntimeException exception, WebRequest request) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(Response.builder().error(true).message(exception.getMessage()).build());
	}

	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<Response> productNotFoundException(ProductNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(Response.builder().error(true).message(exception.getMessage()).build());
	}

	@ExceptionHandler(value = { AccessDeniedException.class })
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException {
		HashMap<String, Object> error = new HashMap<>();
		error.put("error", true);
		error.put("message", "Access Denied");
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		new ObjectMapper().writeValue(response.getOutputStream(), error);
	}

}
