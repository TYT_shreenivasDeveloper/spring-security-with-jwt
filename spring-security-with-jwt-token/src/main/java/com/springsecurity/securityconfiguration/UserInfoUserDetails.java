package com.springsecurity.securityconfiguration;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.springsecurity.entity.UserInfo;

public class UserInfoUserDetails implements UserDetails {

	private String userName;

	private String password;

	private Collection<SimpleGrantedAuthority> authorities;

	public UserInfoUserDetails(UserInfo userInfo) {
		super();
		this.userName = userInfo.getUsername();
		this.password = userInfo.getPassword();
		this.authorities = Arrays.stream(userInfo.getRole().split(",")).map(SimpleGrantedAuthority::new).toList();
	}

	private static final long serialVersionUID = 1L;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
