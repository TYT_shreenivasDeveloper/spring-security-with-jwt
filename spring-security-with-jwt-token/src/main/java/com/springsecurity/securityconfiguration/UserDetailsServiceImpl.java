package com.springsecurity.securityconfiguration;

import static com.springsecurity.constants.UserDetailsConstants.USER_NOT_FOUND_EXCEPTION;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.springsecurity.exception.UserNotFoundException;
import com.springsecurity.repository.UserInfoRepository;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return Optional.ofNullable(userInfoRepository.findByUsername(username)).map(UserInfoUserDetails::new)
				.orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND_EXCEPTION));
	}

}
