package com.springsecurity.entity;

import java.io.Serializable;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.OptimizableGenerator;

import com.springsecurity.util.ProductCustomIdGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Product_Id")
	@GenericGenerator(name = "product_id", strategy = "com.springsecurity.util.ProductCustomIdGenerator", parameters = {
			@Parameter(name = OptimizableGenerator.INCREMENT_PARAM, value = "1"),
			@Parameter(name = ProductCustomIdGenerator.VALUE_PREFIX_PARAMETER, value = "%PID"),
			@Parameter(name = ProductCustomIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%06d") })
	private String productId;

	private String productName;

	private Double productPrice;

	private Boolean isDeleted;

	private Integer productQuantity;

}
