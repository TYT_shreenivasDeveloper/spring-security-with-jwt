package com.springsecurity.service;

import static com.springsecurity.constants.ProductContants.PRODUCT_ADDED_SUCCESSFULLY;
import static com.springsecurity.constants.ProductContants.PRODUCT_DETAILS_DELETED_S_UCCESSFULLY;
import static com.springsecurity.constants.ProductContants.PRODUCT_DETAILS_NOT_FOUND;
import static com.springsecurity.constants.ProductContants.SOMETHING_WENT_WRONG;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.springsecurity.entity.Product;
import com.springsecurity.exception.ProductNotFoundException;
import com.springsecurity.repository.ProductRepository;
import com.springsecurity.request.ProductRequest;
import com.springsecurity.response.ProductResponse;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServiceImp implements ProductService {

	private final ProductRepository productRepository;

	@Override
	public String addProduct(ProductRequest productRequest) {
		Product product = Product.builder().build();
		BeanUtils.copyProperties(productRequest, product);
		productRepository.save(product);
		return PRODUCT_ADDED_SUCCESSFULLY;
	}

	@Override
	public ProductRequest updateProduct(ProductRequest productRequest) {
		try {
			return Optional
					.ofNullable(
							productRepository.findByProductIdAndIsDeleted(productRequest.getProductId(), Boolean.FALSE))
					.map(product -> {
						BeanUtils.copyProperties(productRequest, product);
						Product save = productRepository.save(product);
						BeanUtils.copyProperties(save, productRequest);
						return productRequest;
					}).orElseThrow(() -> new ProductNotFoundException(PRODUCT_DETAILS_NOT_FOUND));
		} catch (ProductNotFoundException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new ProductNotFoundException("Something Went Wrong");
		}
	}

	@Override
	public ProductResponse getProductBasedOnId(String productId) {
		try {
			return Optional.ofNullable(productRepository.findByProductIdAndIsDeleted(productId, Boolean.FALSE))
					.map(product -> {
						ProductResponse productResponse = ProductResponse.builder().build();
						BeanUtils.copyProperties(product, productResponse);
						return productResponse;
					}).orElseThrow(() -> new ProductNotFoundException(PRODUCT_DETAILS_NOT_FOUND));
		} catch (ProductNotFoundException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new ProductNotFoundException(SOMETHING_WENT_WRONG);
		}
	}

	@Override
	public String deleteProductDetails(String productId) {
		try {
			return Optional.ofNullable(productRepository.findByProductIdAndIsDeleted(productId, Boolean.FALSE))
					.map(product -> {
						product.setIsDeleted(Boolean.TRUE);
						return PRODUCT_DETAILS_DELETED_S_UCCESSFULLY;
					}).orElseThrow(() -> new ProductNotFoundException(PRODUCT_DETAILS_NOT_FOUND));
		} catch (ProductNotFoundException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new ProductNotFoundException(SOMETHING_WENT_WRONG);
		}
	}

	@Override
	public List<ProductResponse> getAllProductDetails() {
		try {
			return productRepository.findAllByIsDeleted(Boolean.FALSE).stream().map(products -> {
				ProductResponse productResponse = ProductResponse.builder().build();
				BeanUtils.copyProperties(products, productResponse);
				return productResponse;
			}).toList();
		} catch (ProductNotFoundException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new ProductNotFoundException(SOMETHING_WENT_WRONG);
		}
	}
}
