package com.springsecurity.service;

import static com.springsecurity.constants.UserDetailsConstants.ROLE;
import static com.springsecurity.constants.UserDetailsConstants.USER_REGISTRATION_IS_DONE_SUCCESSFULLY;

import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.springsecurity.entity.UserInfo;
import com.springsecurity.repository.UserInfoRepository;
import com.springsecurity.request.UserRegistrationRequest;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final PasswordEncoder passwordEncoder;

	private final UserInfoRepository userDetailsRepository;

	@Override
	public String userRegisteration(UserRegistrationRequest userRegistrationRequest) {
		UserInfo userDetails = UserInfo.builder().build();
		userDetails.setRole(ROLE + userRegistrationRequest.getRole());
		userDetails.setPassword(passwordEncoder.encode(userRegistrationRequest.getPassword()));
		BeanUtils.copyProperties(userRegistrationRequest, userDetails);
		userDetailsRepository.save(userDetails);
		return USER_REGISTRATION_IS_DONE_SUCCESSFULLY;
	}

}
