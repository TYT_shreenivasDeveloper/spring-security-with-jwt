package com.springsecurity.service;

import java.util.List;

import com.springsecurity.request.ProductRequest;
import com.springsecurity.response.ProductResponse;

public interface ProductService {

	public String addProduct(ProductRequest productRequest);

	public ProductRequest updateProduct(ProductRequest productRequest);

	public ProductResponse getProductBasedOnId(String productId);

	public String deleteProductDetails(String productId);

	public List<ProductResponse> getAllProductDetails();

}
