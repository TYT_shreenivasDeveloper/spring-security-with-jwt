package com.springsecurity.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springsecurity.entity.UserInfo;
import com.springsecurity.repository.UserInfoRepository;
import com.springsecurity.request.LoginRequest;
import com.springsecurity.util.JwtUtils;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

	private final AuthenticationManager authenticationManager;

	private final UserInfoRepository userInfoRepository;

	private final JwtUtils jwtUtils;

	@Override
	public Map<String, Object> userLogin(LoginRequest loginRequest) {
		Authentication authenticate = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUserName(), loginRequest.getPassword()));

		if (authenticate.isAuthenticated()) {
			UserInfo findByUsername = userInfoRepository.findByUsername(loginRequest.getUserName());
			Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority("ROLE_" + findByUsername.getRole()));
			return jwtUtils.generateToken(findByUsername, authorities);
		} else {
			throw new UsernameNotFoundException("invalid user request !");
		}
	}

}
