package com.springsecurity.service;

import java.util.Map;

import com.springsecurity.request.LoginRequest;

public interface AuthenticationService {
	
	public Map<String, Object> userLogin(LoginRequest loginRequest);

}
