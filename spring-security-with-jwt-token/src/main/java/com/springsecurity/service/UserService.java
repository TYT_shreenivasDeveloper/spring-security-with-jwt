package com.springsecurity.service;

import com.springsecurity.request.UserRegistrationRequest;

public interface UserService {
	
	public String userRegisteration(UserRegistrationRequest userRegistrationRequest) ;

}
