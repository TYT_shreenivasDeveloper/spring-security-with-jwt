package com.springsecurity.util;

import java.security.Key;
import java.time.Instant;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.springsecurity.entity.UserInfo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtils {

	private String jwtSecret = "javadeveloper";

	private int jwtExpirationMs = 300000;

	public Map<String, Object> generateToken(UserInfo userInfo, Collection<SimpleGrantedAuthority> authorities) {
		final Key hmacKey = new SecretKeySpec(Base64.getEncoder().encode(jwtSecret.getBytes()),
				SignatureAlgorithm.HS512.getJcaName());
		String accessToken = Jwts.builder()
				.claim("roles", authorities.stream().map(GrantedAuthority::getAuthority).toList())
				.setIssuer("java Developer").setSubject(userInfo.getUsername()).setAudience("you")
				.claim("user_type", userInfo.getRole()).claim("user_id", userInfo.getUserId())
				.setExpiration(new Date(Date.from(Instant.now()).getTime() + jwtExpirationMs))
				.setIssuedAt(Date.from(Instant.now())).setHeaderParam("typ", "JWT").setId(UUID.randomUUID().toString())
				.signWith(hmacKey).compact();
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("userInfo", userInfo);
		response.put("accessToken", accessToken);
		return response;

	}

	public Claims getClaimsFromToken(String token) {
		return Jwts.parserBuilder().setSigningKey(new SecretKeySpec(Base64.getEncoder().encode(jwtSecret.getBytes()),
				SignatureAlgorithm.HS512.getJcaName())).build().parseClaimsJws(token).getBody();
	}

	public String getUsernameFromToken(String token) {
		return getClaimsFromToken(token).getSubject();
	}

	public Date getExpirationDate(String token) {
		return getClaimsFromToken(token).getExpiration();
	}

	public Boolean isTokenExpired(String token) {
		Date expirationDate = getExpirationDate(token);
		return expirationDate.before(new Date());
	}

	public Boolean isTokenValidated(String token) {
		return !isTokenExpired(token);
	}

}
