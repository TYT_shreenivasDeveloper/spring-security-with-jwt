package com.springsecurity.util;

import java.io.IOException;
import java.util.Optional;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.springsecurity.entity.UserInfo;
import com.springsecurity.exception.CustomAccessDeniedHandler;
import com.springsecurity.exception.ProductNotFoundException;
import com.springsecurity.repository.UserInfoRepository;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class AuthorizationFilter extends OncePerRequestFilter {

	private final JwtUtils jwtUtils;

	private final CustomAccessDeniedHandler accessDenied;

	private final UserDetailsService userDetailsService;

	private final UserInfoRepository userInfoRepository;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String header = request.getHeader("Authorization");
		if (header != null && header.startsWith("Bearer ")) {
			try {
				String token = header.substring(7);
				jwtUtils.isTokenValidated(token);
				String usernameFromToken = jwtUtils.getUsernameFromToken(token);
				UserDetails userDetails = userDetailsService.loadUserByUsername(usernameFromToken);
				verification(request, response, usernameFromToken);
				SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
						userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities()));
				filterChain.doFilter(request, response);
			} catch (Exception exception) {
				try {
					accessDenied.handle(request, response, new AccessDeniedException(exception.getMessage()));
				} catch (Exception exception2) {
					throw new ProductNotFoundException("Something Went Wrong");
				}
			}
		} else
			filterChain.doFilter(request, response);
	}

	private void verification(HttpServletRequest request, HttpServletResponse response, String username) {
		UserInfo userInfo = Optional.ofNullable(userInfoRepository.findByUsername(username)).orElse(null);
		if (userInfo == null) {
			try {
				accessDenied.handle(request, response, new AccessDeniedException("Unauthorized Access Token"));
			} catch (Exception exception2) {
				throw new ProductNotFoundException("Something Went Wrong");
			}

		}
	}

}
