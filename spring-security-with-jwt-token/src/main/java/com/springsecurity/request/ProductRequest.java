package com.springsecurity.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {

	private String productId;

	private String productName;

	private Double productPrice;

	private Integer productQuantity;

}
