package com.springsecurity.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationRequest {

	private String username;

	private String password;

	private String emailId;

	private String address;

	private String moblieNumber;

	private String role;

}
