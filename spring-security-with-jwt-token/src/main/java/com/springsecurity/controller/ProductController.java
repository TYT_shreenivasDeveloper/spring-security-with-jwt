package com.springsecurity.controller;

import static com.springsecurity.constants.ProductContants.GETTING_ALL_THE_DETAILS;
import static com.springsecurity.constants.ProductContants.GETTING_THE_DETAILS_BASED_ON_PRODUCT_ID;
import static com.springsecurity.constants.ProductContants.PRODUCT_DETAILS_DELETE_SUCCESSFULLY;
import static com.springsecurity.constants.ProductContants.PRODUCT_DETAILS_UPDATED_SUCCESSFULLY;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springsecurity.request.ProductRequest;
import com.springsecurity.response.Response;
import com.springsecurity.service.ProductService;

import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping("api/v1/product")
public class ProductController {

	private final ProductService productService;

	@PostMapping("/add-product")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<Response> addProductsDetails(@RequestBody ProductRequest productRequest) {
		return ResponseEntity.ok()
				.body(Response.builder().error(Boolean.FALSE).data(productService.addProduct(productRequest)).build());
	}

	@PutMapping("/update-product")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<Response> updateProductDetails(@RequestBody ProductRequest productRequest) {
		return ResponseEntity.ok()
				.body(Response.builder().error(Boolean.FALSE).message(PRODUCT_DETAILS_UPDATED_SUCCESSFULLY)
						.data(productService.updateProduct(productRequest)).build());
	}

	@DeleteMapping("/delete-product/{productId}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<Response> deleteProductDetails(@PathVariable("productId") String productId) {
		return ResponseEntity.ok()
				.body(Response.builder().error(Boolean.FALSE).message(PRODUCT_DETAILS_DELETE_SUCCESSFULLY)
						.data(productService.deleteProductDetails(productId)).build());
	}

	@GetMapping("/product/{productId}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN','ROLE_USER')")
	public ResponseEntity<Response> getProductDetailsBasedOnId(@PathVariable("productId") String productId) {
		return ResponseEntity.ok()
				.body(Response.builder().error(Boolean.FALSE).message(GETTING_THE_DETAILS_BASED_ON_PRODUCT_ID)
						.data(productService.getProductBasedOnId(productId)).build());
	}

	@GetMapping("/products")
	@PreAuthorize("hasAuthority('ROLE_ADMIN','ROLE_USER')")
	public ResponseEntity<Response> getProductDetails() {
		return ResponseEntity.ok().body(Response.builder().error(Boolean.FALSE).message(GETTING_ALL_THE_DETAILS)
				.data(productService.getAllProductDetails()).build());
	}

}
