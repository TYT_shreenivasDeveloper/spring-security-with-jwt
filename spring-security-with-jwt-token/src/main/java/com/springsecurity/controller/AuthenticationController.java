package com.springsecurity.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springsecurity.request.LoginRequest;
import com.springsecurity.response.Response;
import com.springsecurity.service.AuthenticationService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/auth")
public class AuthenticationController {

	private final AuthenticationService authenticationService;

	@PostMapping("user/authentication")
	public ResponseEntity<Response> userLogin(@RequestBody LoginRequest loginRequest) {
		return ResponseEntity.ok().body(
				Response.builder().error(Boolean.FALSE).data(authenticationService.userLogin(loginRequest)).build());

	}

}
