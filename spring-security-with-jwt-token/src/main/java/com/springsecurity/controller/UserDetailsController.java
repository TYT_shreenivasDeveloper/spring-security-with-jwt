package com.springsecurity.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springsecurity.request.UserRegistrationRequest;
import com.springsecurity.response.Response;
import com.springsecurity.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping("api/v1/users")
public class UserDetailsController {

	private final UserService userService;

	@PostMapping("user-registration")
	public ResponseEntity<Response> userRegistration(@RequestBody UserRegistrationRequest userRegistrationRequest) {
		return ResponseEntity.ok().body(Response.builder().error(Boolean.FALSE)
				.data(userService.userRegisteration(userRegistrationRequest)).build());

	}

}
